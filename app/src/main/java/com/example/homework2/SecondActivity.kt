package com.example.homework2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init() {
        save.setOnClickListener {

            if (nameEditText.text.toString().isEmpty() || lastEditText.text.toString().isEmpty()
                || emailEditText.text.toString().isEmpty() || ageEditText.text.toString().isEmpty()
                || genderEditText.text.toString().isEmpty()) {
                Toast.makeText(this, "Please feel all fields", Toast.LENGTH_SHORT).show()
            }
            else {
                openMainActivity()
            }
        }
    }

    private fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("name", nameEditText.text.toString())
        intent.putExtra("last", lastEditText.text.toString())
        intent.putExtra("email", emailEditText.text.toString())
        intent.putExtra("age", ageEditText.text.toString().toInt())
        intent.putExtra("gender", genderEditText.text.toString())
        startActivity(intent)
    }

}
