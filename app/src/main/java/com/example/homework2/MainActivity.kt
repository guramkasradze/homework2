package com.example.homework2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var name = "Guram"
    private var last= "Kasradze"
    private var email = "gurami.kasradze.1@btu.edu.ge"
    private var age = 20
    private var gender = "Male"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        changeInfo.setOnClickListener {
            openSecondActivity()
        }

        val intent = intent.extras

        if (intent != null) {
            name = intent!!.getString("name", "Your Name")
            last = intent.getString("last", "Your Last Name")
            email = intent.getString("email", "Your Email")
            age = intent.getInt("age", 0)
            gender = intent.getString("gender", "")
        }



        nameTextView.text = name
        lastTextView.text = last
        emailTextView.text = email
        ageTextView.text = age.toString()
        genderTextView.text = gender

    }

    private fun openSecondActivity() {
        val intent = Intent(this, SecondActivity::class.java)
        startActivity(intent)
    }
}
